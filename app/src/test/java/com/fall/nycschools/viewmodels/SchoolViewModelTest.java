package com.fall.nycschools.viewmodels;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;

import com.fall.nycschools.models.SAT;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class SchoolViewModelTest {

    @Rule
    public TestRule rule = new InstantTaskExecutorRule();

    private SchoolViewModel schoolViewModel;
    private SAT sat;
    private List<SAT> listScores = null;

    @Before
    public void setUp() throws Exception {

        sat = SAT.getInstance();
        sat.setDbn("21Mp");
        sat.setSchool_name("Bronx High School");
        sat.setNum_of_sat_test_takers("9");
        sat.setSat_critical_reading_avg_score("765");
        sat.setSat_math_avg_score("231");
        sat.setSat_writing_avg_score("254");
    }

    @Test
    public void test_getSAT_nullValue() {
        schoolViewModel = new SchoolViewModel(listScores);
        List<SAT> result = schoolViewModel.getSAT().getValue();
        assertNull(result);
    }

    @Test
    public void test_getSAT_emptyValues() {
        listScores = new ArrayList<>();
        schoolViewModel = new SchoolViewModel(listScores);
        List<SAT> result = schoolViewModel.getSAT().getValue();
        assertTrue(result.isEmpty());
    }

    @Test
    public void test_getSAT_getSchoolName() {
        listScores = new ArrayList<>();
        listScores.add(sat);
        schoolViewModel = new SchoolViewModel(listScores);
        String schoolName = schoolViewModel.getSAT().getValue().get(0).getSchool_name();
        assertEquals(schoolName, "Bronx High School");
    }



}