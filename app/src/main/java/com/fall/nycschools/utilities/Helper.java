package com.fall.nycschools.utilities;

import android.content.Context;
import android.widget.Toast;

public class Helper {

    private Context context;

    public Helper(Context context) {
        this.context = context;
    }

    public void toast(String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }



}
