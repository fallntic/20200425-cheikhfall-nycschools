package com.fall.nycschools.repositories;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.fall.nycschools.services.interfaces.SchoolApi;
import com.fall.nycschools.models.SAT;
import com.fall.nycschools.models.School;
import com.fall.nycschools.services.clients.SchoolClient;
import com.fall.nycschools.utilities.Helper;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

/**
 * This is the repository which fetch data in an API in the background using Asynctask.
 */
public class SchoolRepository {

    private SchoolApi schoolApi;
    private Helper helper;
    private ProgressDialog progressDialog;
    private MutableLiveData<List<SAT>> listSATMutableLiveData;
    private MutableLiveData<List<School>> listSchoolMutableLiveData;

    public SchoolRepository(Context context) {
        helper = new Helper(context);
        initProgressDialog(context);
        listSchoolMutableLiveData = new MutableLiveData<>();
        listSATMutableLiveData = new MutableLiveData<>();
    }

    /**
     * Api call for getting schools' information
     * @return a live data object
     */
    private LiveData<List<School>> getSchoolsRequest() {
        progressDialog.show();
        schoolApi = SchoolClient.getClient().create(SchoolApi.class);
        Call<List<School>> call = schoolApi.getSchool();

        call.enqueue(new Callback<List<School>>() {
            @Override
            public void onResponse(Call<List<School>> call, Response<List<School>> response) {
                if (response.body() != null) {
                    listSchoolMutableLiveData.postValue(response.body());
                    progressDialog.dismiss();
                }
            }
            @Override
            public void onFailure(Call<List<School>> call, Throwable t) {
                Log.d(TAG, t.getMessage());
                helper.toast("Error: " + t.getMessage());
                progressDialog.dismiss();
            }
        });
        return listSchoolMutableLiveData;
    }

    /**
     * Api call for getting SAT Scores information
     * @return a live data object
     */
    private LiveData<List<SAT>> getSATRequest() {
        progressDialog.show();
        schoolApi = SchoolClient.getClient().create(SchoolApi.class);
        Call<List<SAT>> call = schoolApi.getSAT();
        call.enqueue(new Callback<List<SAT>>() {
            @Override
            public void onResponse(Call<List<SAT>> call, Response<List<SAT>> response) {
                if (response.body() != null) {
                    listSATMutableLiveData.postValue(response.body());
                    progressDialog.dismiss();
                }
            }
            @Override
            public void onFailure(Call<List<SAT>> call, Throwable t) {
                Log.d(TAG, t.getMessage());
                helper.toast("Error: " + t.getMessage());
                progressDialog.dismiss();
            }
        });
        return listSATMutableLiveData;
    }


    /**
     * Run the Api request in a background thread to keep the MAinUI free
     * @return a livedata object
     */
    public LiveData<List<School>> getSchools(){
        BackgroundTaskSchool backgroundTaskSchool = new BackgroundTaskSchool();
        return backgroundTaskSchool.doInBackground();
    }

    /**
     * Run the Api request in a background thread to keep the MAinUI free
     * @return a livedata object
     */
    public LiveData<List<SAT>> getSAT(){
        BackgroundTaskSAT backgroundTaskSAT = new BackgroundTaskSAT();
        return backgroundTaskSAT.doInBackground();
    }

    //******************************** AsyncTask creations******************************************
    @SuppressLint("StaticFieldLeak")
    private class BackgroundTaskSchool extends AsyncTask<Void, Void, LiveData<List<School>>> {

        public BackgroundTaskSchool(){}
        @Override
        protected LiveData<List<School>> doInBackground(Void... voids) {
            return getSchoolsRequest();
        }
    }

    private class BackgroundTaskSAT extends AsyncTask<Void, Void, LiveData<List<SAT>>> {

        public BackgroundTaskSAT(){}
        @Override
        protected LiveData<List<SAT>> doInBackground(Void... voids) {
            return getSATRequest();
        }
    }

    private void initProgressDialog(Context context) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setMax(100);
        progressDialog.setMessage("Loading data...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }
}
