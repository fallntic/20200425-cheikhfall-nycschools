package com.fall.nycschools.ui.fragments;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fall.nycschools.R;
import com.fall.nycschools.adapters.SATAdapter;
import com.fall.nycschools.models.SAT;
import com.fall.nycschools.viewmodels.SchoolViewModel;
import com.fall.nycschools.viewmodels.ViewModelFactory;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class SATFragment extends Fragment {

    private RecyclerView recyclerView;
    private SchoolViewModel schoolViewModel;

    public SATFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sat, container, false);
        recyclerView = view.findViewById(R.id.recyclerview);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //Initializing viewmodel
        schoolViewModel = new ViewModelProvider(this, new ViewModelFactory(requireActivity(),
                this)).get(SchoolViewModel.class);
    }

    @Override
    public void onStart() {
        super.onStart();
        getSATResults();
    }

    //Observing schoolViewModel for UI update
    private void getSATResults(){
        schoolViewModel.fetchSATData();
        schoolViewModel.getSAT().observe(this, new Observer<List<SAT>>() {
            @Override
            public void onChanged(List<SAT> sats) {
                showSATScores(sats);
            }
        });
    }

    //Set and show recycler views
    private void showSATScores(List<SAT> sats) {
        SATAdapter satAdapter = new SATAdapter(getContext(), sats);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(satAdapter);
    }
}
