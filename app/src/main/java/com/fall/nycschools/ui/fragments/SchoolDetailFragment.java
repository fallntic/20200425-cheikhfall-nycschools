package com.fall.nycschools.ui.fragments;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fall.nycschools.R;
import com.fall.nycschools.models.School;

/**
 * A simple {@link Fragment} subclass.
 */
public class SchoolDetailFragment extends Fragment {

    private TextView textViewSchoolNAme;
    private TextView textViewSchoolLocation;
    private TextView textViewSchoolPhoneNumber;
    private TextView textViewSchoolEmail;
    private TextView textViewSchoolOverview;

    private School school;

    public SchoolDetailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_school_detail, container, false);

        initViews(view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        school = School.getInstance();
        displayViews();
    }

    private void initViews(View view){
        textViewSchoolNAme = view.findViewById(R.id.tv_school_name);
        textViewSchoolLocation = view.findViewById(R.id.tv_school_location);
        textViewSchoolPhoneNumber = view.findViewById(R.id.tv_school_number);
        textViewSchoolEmail = view.findViewById(R.id.tv_school_email);
        textViewSchoolOverview = view.findViewById(R.id.tv_school_overview);
    }

    private void displayViews(){
        textViewSchoolNAme.setText(school.getSchool_name());
        String primaryAddress = school.getPrimary_address_line_1();
        String city = school.getCity();
        String zip = school.getZip();
        String stateCode = school.getState_code();
        String location = primaryAddress + " " + city + ", " + zip + " " + stateCode;
        textViewSchoolLocation.setText(location);
        textViewSchoolPhoneNumber.setText("Phone number: " + school.getPhone_number());
        textViewSchoolEmail.setText("Email: " + school.getSchool_email());
        textViewSchoolOverview.setText(school.getOverview_paragraph());
    }
}
