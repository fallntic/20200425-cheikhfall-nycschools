package com.fall.nycschools.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.fall.nycschools.R;
import com.fall.nycschools.models.School;

import java.util.List;

/**
 * This adapter will handle displaying NYC school in its recycler view.
 */
public class SchoolAdapter extends RecyclerView.Adapter<SchoolAdapter.SchoolViewHolder>{

    private List<School> schools;
    private final Context context;
    private final RecyclerItemClickListener listener;

    public SchoolAdapter(Context context, List<School> schools, RecyclerItemClickListener listener) {
        this.context = context;
        this.schools = schools;
        this.listener = listener;
    }

    @NonNull
    @Override
    public SchoolViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new SchoolViewHolder(
                LayoutInflater.from(context).inflate(R.layout.item_school, parent, false)
        );
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull SchoolViewHolder holder, int position) {
        School school = schools.get(position);
        holder.textViewSchoolNAme.setText(school.getSchool_name());
        String primaryAddress = school.getPrimary_address_line_1();
        String city = school.getCity();
        String zip = school.getZip();
        String stateCode = school.getState_code();
        String location = primaryAddress + " " + city + ", " + zip + " " + stateCode;
        holder.textViewSchoolLocation.setText(location);

        holder.bind(school, listener);
    }

    @Override
    public int getItemCount() {
        return schools.size();
    }

    public void removeItem(int position) {
        schools.remove(position);
        notifyItemRemoved(position);
    }

    class SchoolViewHolder extends RecyclerView.ViewHolder {

        private TextView textViewSchoolNAme;
        private TextView textViewSchoolLocation;

        SchoolViewHolder(View itemView) {
            super(itemView);

            textViewSchoolNAme = itemView.findViewById(R.id.tv_school_name);
            textViewSchoolLocation = itemView.findViewById(R.id.tv_school_location);
        }

        void bind(final School school, final RecyclerItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClickListener(school, getLayoutPosition());
                }
            });
        }
    }

    public interface RecyclerItemClickListener {
        void onClickListener(School school, int position);
    }

}
