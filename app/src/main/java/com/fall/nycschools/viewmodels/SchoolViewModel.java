package com.fall.nycschools.viewmodels;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.fall.nycschools.models.SAT;
import com.fall.nycschools.models.School;
import com.fall.nycschools.repositories.SchoolRepository;

import java.util.List;

/**
 * This is my ViewModel, it has a reference of the SchoolRepository. I returns live data objects
 * that will be observed by the views to update their UI.
 */
public class SchoolViewModel extends ViewModel {

    private SchoolRepository schoolRepository;
    private MutableLiveData<List<School>> mutableLiveDataSchool;
    private MutableLiveData<List<SAT>> mutableLiveDataSAT;

    public SchoolViewModel(Context context) {
        schoolRepository = new SchoolRepository(context);
    }


    //This constructor is basically used for test purposes
    public SchoolViewModel(List<SAT> list){
        mutableLiveDataSAT = new MutableLiveData<>();
        mutableLiveDataSAT.setValue(list);
    }

    public void fetchSchoolData() {
        mutableLiveDataSchool = (MutableLiveData<List<School>>) schoolRepository.getSchools();
    }

    public LiveData<List<School>> getSchools() {
        return this.mutableLiveDataSchool;
    }

    public void fetchSATData() {
        mutableLiveDataSAT = (MutableLiveData<List<SAT>>) schoolRepository.getSAT();
    }

    public LiveData<List<SAT>> getSAT() {
        return this.mutableLiveDataSAT;
    }


}
