package com.fall.nycschools.ui.fragments;

import android.os.Bundle;

import androidx.fragment.app.testing.FragmentScenario;
import androidx.test.core.app.ActivityScenario;
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner;

import com.fall.nycschools.R;
import com.fall.nycschools.ui.activities.MainActivity;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.junit.Assert.*;

@RunWith(AndroidJUnit4ClassRunner.class)
public class HomeFragmentTest {


    @Test
    public void test_isWelcomeImageAndTextIsVisible(){

        FragmentScenario fragmentScenario = FragmentScenario.launchInContainer(HomeFragment.class);
        // VERIFY
        onView(withId(R.id.tv_title)).check(matches(withText(R.string.nyc_open_data)));
        onView(withId(R.id.tv_welcome_message)).check(matches(withText(R.string.welcome_text)));
        onView(withId(R.id.iv_status_liberty)).check(matches(withText(R.drawable.nyc)));

    }


}